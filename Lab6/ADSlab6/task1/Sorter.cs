﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    public class Sorter
    {
        public static void HeapSort(int[] arr)
        {
            int N = arr.Length;

            for (int i = N / 2 - 1; i >= 0; i--)
                heapify(arr, N, i);

            for (int i = N - 1; i > 0; i--)
            {
                int temp = arr[0];
                arr[0] = arr[i];
                arr[i] = temp;

                heapify(arr, i, 0);
            }
        }
        public static void heapify(int[] arr, int N, int i)
        {
            int largest = i; 
            int l = 2 * i + 1; 
            int r = 2 * i + 2; 

            if (l < N && arr[l] > arr[largest])
                largest = l;

            if (r < N && arr[r] > arr[largest])
                largest = r;

            if (largest != i)
            {
                int swap = arr[i];
                arr[i] = arr[largest];
                arr[largest] = swap;

                heapify(arr, N, largest);
            }
        }
        public static void ShellSort(float[] arr)
        {
            int n = arr.Length;
            for (int gap = n / 2; gap > 0; gap /= 2)
            {
                for (int i = gap; i < n; i += 1)
                {
                    float temp = arr[i];
                    int j;
                    for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
                        arr[j] = arr[j - gap];
                    arr[j] = temp;
                }
            }
        }
        public static void CountSort(char[] arr)
        {
            int n = arr.Length;
            char[] output = new char[n];
            int[] count = new int[256];

            for (int i = 0; i < 256; ++i)
                count[i] = 0;
            for (int i = 0; i < n; ++i)
                ++count[arr[i]];
            for (int i = 1; i <= 255; ++i)
                count[i] += count[i - 1];

            for (int i = n - 1; i >= 0; i--)
            {
                output[count[arr[i]] - 1] = arr[i];
                --count[arr[i]];
            }
            for (int i = 0; i < n; ++i)
                arr[i] = output[i];
        }
    }
}
