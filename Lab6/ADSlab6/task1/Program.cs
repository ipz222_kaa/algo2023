﻿using System.Diagnostics;
using task1;

Stopwatch stopwatch = new();
int[] heapSort = new int[10000];
float[] Shellsort = new float[10000];
char[] countingSort = new char[10000];
Random rand = new();
for (int i = 0; i < heapSort.Length; i++)
{
    heapSort[i] =  rand.Next(100);
    Shellsort[i] = (float)(rand.Next(300) + rand.NextDouble());
    int n = rand.Next(-200, 10);
    if (n < 0)
        n = 256-Math.Abs(n);
    countingSort[i] = (char)n;
}
stopwatch.Start();
Sorter.HeapSort(heapSort);
stopwatch.Stop();
Console.WriteLine($"HeapSort time: {stopwatch.Elapsed.TotalMilliseconds} ms");
stopwatch.Restart();
Sorter.ShellSort(Shellsort);
stopwatch.Stop();
Console.WriteLine($"ShellSort time: {stopwatch.Elapsed.TotalMilliseconds} ms");
stopwatch.Restart();
Sorter.CountSort(countingSort);
stopwatch.Stop();
Console.WriteLine($"CountSort time: {stopwatch.Elapsed.TotalMilliseconds} ms");
