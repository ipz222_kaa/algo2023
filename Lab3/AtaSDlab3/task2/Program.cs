﻿using System.Diagnostics;

Console.InputEncoding = System.Text.Encoding.UTF8;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Лабораторна робта №3. Завдання 2. Варіант 2.\nКупрієнко А. А. ІПЗ 22-2[1]");
Stopwatch stopwatch = new Stopwatch();
int[] octalDigits = new int[20];
int a;
Console.WriteLine("Задача 1.");
stopwatch.Start();
while (true)
{
    Console.Write("Введіть число a (0 <= a <= 20): ");
    if (int.TryParse(Console.ReadLine(), out a) && a >= 0 && a <= 20)
    {
        break;
    }
    else
    {
        Console.WriteLine("Неправильне значення. Спробуйте ще раз.");
    }
}
long factorial = Factorial(a);
stopwatch.Stop();
Console.WriteLine($"Факторіал числа {a} дорівнює {factorial}");
Console.WriteLine($"Час виконання програми: {stopwatch.Elapsed.TotalMilliseconds} мс");
Console.WriteLine("Задача 2.");
stopwatch.Start();
Random rnd = new Random();
for (int i = 0; i < octalDigits.Length; i++)
{
    octalDigits[i] = rnd.Next(8);
}
Console.WriteLine("Випадкові цифри вісімкової системи числення:");
Console.WriteLine(string.Join(" ", octalDigits));
Array.Sort(octalDigits);
Array.Reverse(octalDigits);
string maxNumber = string.Join("", octalDigits);
long longMaxNumber = Convert.ToInt64(maxNumber, 8);
stopwatch.Stop();
Console.Write($"Найбільше можливе число, яке можна утворити з випадкових цифр: {longMaxNumber}");
Console.WriteLine($"Час виконання програми: {stopwatch.Elapsed.TotalMilliseconds} мс");
static long Factorial(int n)
{
    if (n == 0)
    {
        return 1;
    }
    else
    {
        return n * Factorial(n - 1);
    }
}