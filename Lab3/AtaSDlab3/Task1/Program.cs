﻿Console.InputEncoding = System.Text.Encoding.UTF8;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Лабораторна робта №3. Завдання 1. \nКупрієнко А. А. ІПЗ 22-2[1]");
long iter = 50;
Console.WriteLine("\nФункція f(n) = n\n");
for (int i = 0; i <= iter; i++)
{
    Console.WriteLine($"f({i}): {i}");
}
Console.WriteLine("\nФункція f(n) = log(n)\n");
for (int i = 0; i <= iter; i++)
{
    Console.WriteLine($"f({i}): {Math.Log(i)}");
}
Console.WriteLine("\nФункція f(n) = n*log(n)\n");
for (int i = 0; i <= iter; i++)
{
    Console.WriteLine($"f({i}): {i * Math.Log(i)}");
}
Console.WriteLine("\nФункція f(n) = n*n\n");
for (int i = 0; i <= iter; i++)
{
    Console.WriteLine($"f({i}): {Math.Pow(i, 2)}");
}
Console.WriteLine("\nФункція f(n) = 2^n\n");
for (int i = 0; i <= iter; i++)
{
    Console.WriteLine($"f({i}): {Math.Pow(2, i)}");
}
Console.WriteLine("\nФункція f(n) = n!\n");
for (ulong i = 0; i <= (ulong)iter; i++)
{
    Console.WriteLine($"f({i}): {CalculateFactorial(i)}");
}
Console.ReadKey();
static ulong CalculateFactorial(ulong n)
{
    ulong factorial = 1;
    for (ulong i = 2; i <= n; i++)
    {
        factorial *= i;
    }

    return factorial;
}