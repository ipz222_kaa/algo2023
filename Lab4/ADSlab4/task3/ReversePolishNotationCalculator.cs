﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    public class ReversePolishNotationCalculator
    {
        public static double Calculate(string expression)
        {
            Stack<double> stack = new Stack<double>();
            string[] tokens = expression.Split(' ');

            foreach (string token in tokens)
            {
                if (double.TryParse(token, out double number))
                {
                    stack.Push(number);
                }
                else
                {
                    if (token == "sqrt")
                    {
                        if (stack.Count < 1)
                        {
                            Console.WriteLine("Error: Insufficient operands for the operator.");
                            return 0;
                        }

                        double operand = stack.Pop();
                        double result = Math.Sqrt(operand);
                        stack.Push(result);
                    }
                    else
                    {
                        if (stack.Count < 2)
                        {
                            Console.WriteLine("Error: Insufficient operands for the operator.");
                            return 0;
                        }

                        double operand2 = stack.Pop();
                        double operand1 = stack.Pop();
                        double result;

                        switch (token)
                        {
                            case "+":
                                result = operand1 + operand2;
                                break;
                            case "-":
                                result = operand1 - operand2;
                                break;
                            case "*":
                                result = operand1 * operand2;
                                break;
                            case "/":
                                result = operand1 / operand2;
                                break;
                            case "^":
                                result = Math.Pow(operand1, operand2);
                                break;
                            default:
                                Console.WriteLine("Error: Invalid token: " + token);
                                return 0;
                        }

                        stack.Push(result);
                    }
                }
            }

            if (stack.Count == 1)
                return stack.Pop();

            Console.WriteLine("Error: Invalid expression.");
            return 0;
        }
    }
}
