﻿using task3;

Console.InputEncoding = System.Text.Encoding.UTF8;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Лабораторна робта №4. Завдання 3 \nКупрієнко А. А. ІПЗ 22-2[1]");
Console.WriteLine("Enter an expression to calculate using the Reverse Polish Notation" +
        "(make sure to separate the operands by a space):");
try
{
    double res = ReversePolishNotationCalculator.Calculate(Console.ReadLine());
    Console.WriteLine($"Result: {res}");
}
catch(Exception ex)
{
    Console.WriteLine($"Error: {ex.Message}");
}