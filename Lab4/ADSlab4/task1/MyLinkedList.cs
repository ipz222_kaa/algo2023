﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    public class MyLinkedList<T>
    {
        private NodeForMyLinkedList<T> head;
        private NodeForMyLinkedList<T> tail;

        public int Count { get; private set; }

        public MyLinkedList()
        {
            head = null;
            tail = null;
            Count = 0;
        }
        public void AddFirst(T data)
        {
            NodeForMyLinkedList<T> newNode = new NodeForMyLinkedList<T>(data);

            if (head == null)
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                head.Next = newNode;
                newNode.Previous = head;
                head = newNode;
            }

            Count++;
        }
        public void AddLast(T data)
        {
            NodeForMyLinkedList<T> newNode = new NodeForMyLinkedList<T>(data);

            if (tail == null)
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                tail.Previous = newNode;
                newNode.Next = tail;
                tail = newNode;
            }

            Count++;
        }
        public void RemoveFirst()
        {
            if (head == null)
                return;

            if (head == tail)
            {
                head = null;
                tail = null;
            }
            else
            {
                head.Next = head;
                head = head.Previous;
                head.Next = null;
            }

            Count--;
        }
        public void RemoveLast()
        {
            if (tail == null)
                return;

            if (head == tail)
            {
                head = null;
                tail = null;
            }
            else
            {
                tail.Previous = tail;
                tail = tail.Next;
                tail.Previous = null;
            }

            Count--;
        }
        public void PrintList()
        {
            NodeForMyLinkedList<T> current = tail;
            Console.Write("Your List = {");
            while (current != null)
            {
                Console.Write($"{current.Data,3}");
                current = current.Next;
            }
            Console.Write("  }\n");
        }
    }
}
