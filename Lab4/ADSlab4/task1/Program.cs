﻿using task1;

Console.InputEncoding = System.Text.Encoding.UTF8;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Лабораторна робта №4. Завдання 1-2 \nКупрієнко А. А. ІПЗ 22-2[1]");
int number;
bool stopLoop = false;
MyLinkedList<int> linkedList = null;
while (true)
{
    Console.WriteLine("Choose operation:\n0. Create a new List\n1. Add First\n2. Add Last\n3. Remove First\n4. Remove Last" +
        "\n5. Print\n6. Delete current list\n" +
        "Any other number to leave");
    switch (int.Parse(Console.ReadLine()))
    {
        case 0:
            linkedList = new(); break;
        case 1:
            if (linkedList != null)
            {
                Console.Write("Enter a number to add first to the list:");
                linkedList.AddFirst(int.Parse(Console.ReadLine()));
            }
            else
                Console.WriteLine("Create a List first");
            break;
        case 2:
            if (linkedList != null)
            {
                Console.Write("Enter a number to add last to the list:");
                linkedList.AddLast(int.Parse(Console.ReadLine()));
            }
            else
                Console.WriteLine("Create a List first");
            break;
        case 3:
            if (linkedList != null)
            {
                linkedList.RemoveFirst();
            }else
                Console.WriteLine("Create a List first");
            break;
        case 4:
            if (linkedList != null)
            {
                linkedList.RemoveLast();
            }
            else
                Console.WriteLine("Create a List first");
            break;
        case 5:
            if (linkedList != null)
            {
                linkedList.PrintList();
            }else
                Console.WriteLine("Create a List first");
            break;
        case 6:
            if(linkedList!=null)
            {
                linkedList = null;
                Console.WriteLine("Current list deleted, make sure to create a new one before proceding with any other operations");
            }else
                Console.WriteLine("Create a List first");
            break;
        default:
            stopLoop = true;
            break;           
    }
    if (stopLoop == true)
    {
        break;
    }
}
