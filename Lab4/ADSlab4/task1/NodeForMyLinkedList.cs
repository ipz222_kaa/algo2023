﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    public class NodeForMyLinkedList<T>
    {
        public T Data { get; set; }
        public NodeForMyLinkedList<T> Next { get; set; }
        public NodeForMyLinkedList<T> Previous { get; set; }

        public NodeForMyLinkedList(T data)
        {
            Data = data;
            Next = null;
            Previous = null;
        }
    }
}
