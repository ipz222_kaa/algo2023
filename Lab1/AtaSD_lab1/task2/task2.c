#include <stdio.h>
#include <stdint.h>
#include <math.h>
union MyUnion
{
    struct
    {
        uint16_t b0 : 1;
        uint16_t b1 : 1;
        uint16_t b2 : 1;
        uint16_t b3 : 1;
        uint16_t b4 : 1;
        uint16_t b5 : 1;
        uint16_t b6 : 1;
        uint16_t b7 : 1;
        uint16_t b8 : 1;
        uint16_t b9 : 1;
        uint16_t b10 : 1;
        uint16_t b11 : 1;
        uint16_t b12 : 1;
        uint16_t b13 : 1;
        uint16_t b14 : 1;
        uint16_t b15 : 1;
    }var1;
    int16_t var2;
};
void main()
{
    system("chcp 1251"); system("cls");
    printf("����������� ������ �1. �������� 2.\n������� �. �. ��� 22-2[1]\n");
    signed short inputValue, inputValueCalc = 0;
    union MyUnion var;
    printf("������ ��������: "); scanf_s("%hd", &inputValue);
    printf("-------��������������� <union> �� <struct>-------\n");
    var.var2 = inputValue;
    int temparray[] = {var.var1.b0, var.var1.b1, var.var1.b2, var.var1.b3, var.var1.b4, var.var1.b5,
    var.var1.b6, var.var1.b7, var.var1.b8, var.var1.b9, var.var1.b10, var.var1.b11, var.var1.b12,
    var.var1.b13, var.var1.b14 };
    if (var.var1.b15 == 1)
    {
        printf("������� �������� � ��'�����\n");
      
        for (int i = 0; i < 15; i++)
        {
            if (temparray[i] == 0)
                temparray[i] = 1;
            else temparray[i] = 0;
        }
        for (int i = 0; i < 15; i++)
        {
            if (temparray[i] == 1)
            {
                inputValueCalc += (temparray[i] * pow(2, i));
            }
        }
        inputValueCalc++;
        inputValueCalc *= -1;
    }
    else {
        printf("������� �������� � �������� ��� 0\n");
        for (int i = 0; i < 15; i++)
        {
            if (temparray[i] == 1)
            {
                inputValueCalc += (temparray[i] * pow(2, i));
            }
        }
    }
    printf("������� �������� = %d\n", inputValueCalc);
    printf("-------���������������� ������ ��������-------\n");
    if ((inputValue >> 15) - (-inputValue >> 15) == 1)
        printf("������� �������� �������\n");
    else if ((inputValue >> 15) - (-inputValue >> 15) == -1)
        printf("������� �������� ��'����\n");
    else printf("������� �������� ������� ����\n");
    for (signed short i = -128; i < 128; i++)
    {
        if ((i ^ inputValue) == 0)
        {
            printf("������� �������� = %d", i);
        }
    }
}