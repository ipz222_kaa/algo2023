#include <stdio.h>
#include <time.h>
struct dateAndTime
{
    short hour : 6;
    short minute : 8;
    short second : 8;
    short year : 12;
    short month : 5;
    short dayNumber : 6;
};
void printDate(struct dateAndTime date)
{
    printf("%hd.%hd.%hd\n", date.dayNumber, date.month, date.year);
}
void printTime(struct dateAndTime dateAndTime)
{
    printf("%hd:%hd:%hd\n", dateAndTime.hour, dateAndTime.minute, dateAndTime.second);
}
void main()
{
    system("chcp 1251"); system("cls");
    printf("����������� ������ �1. �������� 1.\n������� �. �. ��� 22-2[1]\n");
    struct dateAndTime dateAndTime;
    short year, month, day, hour, minute, second;
    printf("������ ����:\n");
    printf("������ ��: "); scanf_s("%hd", &year);
    dateAndTime.year = year;
    printf("������ ����� ����(�����): "); scanf_s("%hd", &month);
    dateAndTime.month = month;
    printf("������ ���� �����: "); scanf_s("%hd", &day);
    dateAndTime.dayNumber = day;
    printf("������ ���:\n");
    printf("������ ������: "); scanf_s("%hd", &hour);
    dateAndTime.hour = hour;
    printf("������ �������: "); scanf_s("%hd", &minute);
    dateAndTime.minute = minute;
    printf("������ �������: "); scanf_s("%hd", &second);
    dateAndTime.second = second;
    printDate(dateAndTime);
    printTime(dateAndTime);
    printf("Size of MyStruct = %d\nSize of tm = %d\n", sizeof(dateAndTime), sizeof(struct tm));
}