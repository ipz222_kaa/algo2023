﻿using Project;

Console.InputEncoding = System.Text.Encoding.UTF8;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Лабораторна робта №7-8. Купрієнко А. А. ІПЗ 22-2[1]");
Tree tree = new(new Node(0, "Київ"));
Node[] nodes =
{
    new Node(135, "Житомир"),
    new Node(80, "Новоград-Волинський"),
    new Node(100, "Рівно"),
    new Node(68, "Луцьк"),
    new Node(38, "Бердичів"),
    new Node(73, "Вінниця"),
    new Node(110, "Хмельницький"),
    new Node(104, "Тернопіль"),
    new Node(115, "Шепетівка"),
    new Node(78, "Біла Церква"),
    new Node(115, "Умань"),
    new Node(146, "Черкаси"),
    new Node(105, "Кременчук"),
    new Node(181, "Полтава"),
    new Node(130, "Харків"),
    new Node(128, "Прилуки"),
    new Node(175, "Суми"),
    new Node(109, "Миргород")

};
string[] parentNames =
{
    "Київ",
    "Житомир",
    "Новоград-Волинський",
    "Рівно",
    "Житомир",
    "Бердичів",
    "Вінниця",
    "Хмельницький",
    "Житомир",
    "Київ",
    "Біла Церква",
    "Біла Церква",
    "Черкаси",
    "Біла Церква",
    "Полтава",
    "Київ",
    "Прилуки",
    "Прилуки"
};
for (int i = 0; i < nodes.Length; i++)
{
    tree.InsertNode(nodes[i], parentNames[i]);
}
Console.WriteLine("Breadth first search results:");
tree.BFS();
Console.WriteLine("\nDepth first search results:");
tree.DFS();
int[,] adjacencyMatrix = tree.CreateAdjacencyMatrix();
Console.WriteLine("\nМатриця суміжності:\n");
for (int i = 0; i < adjacencyMatrix.GetLength(0); i++)
{
    for (int j = 0; j < adjacencyMatrix.GetLength(1); j++)
        Console.Write($"{adjacencyMatrix[i, j], 3}  ");
    Console.WriteLine();
}
tree.WriteRoutsAndSums();