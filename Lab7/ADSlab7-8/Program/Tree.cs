﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class Tree
    {
        public Node Root { private set; get; }
        public List<Node> allChildren { get; private set; }
        public Tree(Node root)
        {
            Root = root;
            allChildren = new();
        }
        public string InsertNode(Node node, string parentName)
        {
            if (parentName == "Київ")
            {
                Root.Children.Add(node);
                node.Parent = Root;
                allChildren.Add(node);
                return "Added new child to root";
            }
            else
            {
                for (int i = 0; i < allChildren.Count; i++)
                {
                    if (allChildren[i].Name == parentName)
                    {
                        allChildren[i].Children.Add(node);
                        node.Parent = allChildren[i];
                        allChildren.Add(node);
                        return "Added new node";
                    }
                }
                return "Parent node not found";
            }
        }
        public void BFS()
        {
            Queue<Node> queue = new();//Ініціалізуємо чергу для реалізації алгоритму
            queue.Enqueue(Root);//Додаємо корінь дерева як початокову вершину графу до черги
            while (queue.Count != 0)//Перевірка на наявність не перевірених елементів графу
            {
                Node node = queue.Dequeue();//Дістаємо перший елемент із черги
                Console.Write(node.Name + " -> ");// і виводимо назву вершини консоль
                foreach (Node item in node.Children)// проходимо по усім дітям вершини, яку ми взяли з черги
                {
                    if (!item.isVisited)// і перевіряємо чи відвідували ми вже ці вершини
                    {
                        item.isVisited = true;//Якщо ж так, то помічаємо що вершина була відвідана/використана
                        queue.Enqueue(item);// та додаємо її у чергу для перевірки її дітей
                    }// Якщо ж ні, переходимо до наступної вершини
                }
            }// Повторюється поки усі вершини не будуть відвідані
            foreach (var item in allChildren)// Після завершення проходу по графу, проходимо знову по усім вершинам
                item.isVisited = false;// і змінюємо властивість вершин на "не відвідані" щоб за потреби повторити операцію
        }
        public void DFS()
        {
            Stack<Node> stack = new Stack<Node>();//Ініціалізуємо стек для реалізації алгоритму
            stack.Push(Root);//Додаємо корінь графу/дерева у стек

            while (stack.Count > 0)//На кожній ітерації перевіряємо чи є ще не відвідані вершини
            {
                Node current = stack.Pop();//Дістаємо верхній елемент зі стеку

                if (!current.isVisited)//Перевіряємо чи ми ще не відвідували цю вершину
                {
                    Console.Write(current.Name + " -> ");//Виводимо назву вершини у консоль
                    current.isVisited = true;// Помічаємо вершину як пройдену/відвідану/використану
                    if (current.Children.Count > 0)//Перевіряємо чи має вершина дітей
                    {//Якщо так, то проходимо циклом по ним у зворотньому порядку
                        for(int i = current.Children.Count-1;i>=0;i--)//Зворотній порядок та цикл for замість foreach
                        {// використовується для коректного проходу по дітям вершин через особливості роботи стеку
                            if (!current.Children[i].isVisited)// Перевіряємо чи відвідані ці вершинм
                                stack.Push(current.Children[i]);// і додаємо їх у стек
                        }
                    }//Якщо ні, переходимо до наступної ітерації, тобто наступної вершини
                }
            }
            foreach (var item in allChildren)// Після завершення проходу по графу, проходимо знову по усім вершинам
                item.isVisited = false;// і змінюємо властивість вершин на "не відвідані" щоб за потреби повторити операцію
        }
        public int[,] CreateAdjacencyMatrix()
        {
            List<Node> allNodes = new List<Node>();
            CollectAllNodes(Root, allNodes);
            int[,] adjacencyMatrix = new int[allNodes.Count, allNodes.Count];

            for (int i = 0; i < allNodes.Count; i++)
            {
                Node node = allNodes[i];
                for (int j = 0; j < allNodes.Count; j++)
                {
                    Node otherNode = allNodes[j];
                    if (node.Children.Contains(otherNode))
                    {
                        adjacencyMatrix[i, j] = otherNode.Value; 
                    }
                    else
                    {
                        adjacencyMatrix[i, j] = 0; 
                    }
                }
            }
            return adjacencyMatrix;
        }

        private void CollectAllNodes(Node node, List<Node> allNodes)
        {
            allNodes.Add(node);

            foreach (Node child in node.Children)
            {
                CollectAllNodes(child, allNodes);
            }
        }
        public void WriteRoutsAndSums()
        {
            Console.WriteLine("Можиливі маршрути:");
            Console.WriteLine("-----------------------------------");

            TraverseTree(Root, new List<string>(), 0);
        }

        private void TraverseTree(Node node, List<string> destinations, int sum)
        {
            destinations.Add(node.Name);
            sum += node.Value;
            if (destinations.Count > 1)
            {
                Console.WriteLine("Маршрут: " + string.Join(" -> ", destinations));
                destinations.RemoveAt(destinations.Count - 1);
                Console.WriteLine("Відстань всього маршруту: " + sum);
                Console.WriteLine();
            }

            foreach (Node child in node.Children)
            {
                TraverseTree(child, destinations, sum);
            }

            sum -= node.Value;
        }
    }
}