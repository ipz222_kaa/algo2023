﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class Node
    {
        public int Value { set; get; }
        public string Name { set; get; }
        public Node Parent { set; get; }
        public List<Node> Children { set; get; }
        public bool isVisited { set; get; }
        public Node(int value, string name)
        {
            Value = value;
            Name = name;
            Parent = this;
            Children = new();
            isVisited = false;
        }
    }
}
