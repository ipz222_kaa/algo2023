﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    public class Sorter
    {
        public static void InsertionSort(MyLinkedList list)
        {
            if (list == null || list.Count <= 1)
                return;

            NodeForMyLinkedList current = list.tail.Next;

            while (current != null)
            {
                int valueToInsert = current.Data;
                NodeForMyLinkedList innerCurrent = current.Previous;

                while (innerCurrent != null && innerCurrent.Data > valueToInsert)
                {
                    innerCurrent.Next.Data = innerCurrent.Data;
                    innerCurrent = innerCurrent.Previous;
                }

                if (innerCurrent == null)
                    list.tail.Data = valueToInsert;
                else
                    innerCurrent.Next.Data = valueToInsert;

                current = current.Next;
            }
        }
        public static void InsertionSort(int[] arr)
        {
            int key;
            for (int i = 0; i < arr.Length; i++)
            {
                key = arr[i];
                int j = i - 1;
                while (j >= 0 && arr[j] > key)
                {
                    arr[j + 1] = arr[j];
                    j = j - 1;
                }
                arr[j + 1] = key;
            }
        }
    }

}
