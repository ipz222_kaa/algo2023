﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace task1
{
    public class MyLinkedList
    {
        public NodeForMyLinkedList head;
        public NodeForMyLinkedList tail;

        public int Count { get; private set; }

        public MyLinkedList()
        {
            head = null;
            tail = null;
            Count = 0;
        }
        public void AddFirst(int data)
        {
            NodeForMyLinkedList newNode = new NodeForMyLinkedList(data);

            if (head == null)
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                head.Next = newNode;
                newNode.Previous = head;
                head = newNode;
            }

            Count++;
        }
        public void AddLast(int data)
        {
            NodeForMyLinkedList newNode = new NodeForMyLinkedList(data);

            if (tail == null)
            {
                head = newNode;
                tail = newNode;
            }
            else
            {
                tail.Previous = newNode;
                newNode.Next = tail;
                tail = newNode;
            }

            Count++;
        }
        public void RemoveFirst()
        {
            if (head == null)
                return;

            if (head == tail)
            {
                head = null;
                tail = null;
            }
            else
            {
                head.Next = head;
                head = head.Previous;
                head.Next = null;
            }

            Count--;
        }
        public void RemoveLast()
        {
            if (tail == null)
                return;

            if (head == tail)
            {
                head = null;
                tail = null;
            }
            else
            {
                tail.Previous = tail;
                tail = tail.Next;
                tail.Previous = null;
            }

            Count--;
        }
        public void PrintList()
        {
            NodeForMyLinkedList current = tail;
            Console.Write("Your List = {");
            while (current != null)
            {
                Console.Write($"{current.Data,3}");
                current = current.Next;
            }
            Console.Write("  }\n");
        }
        public void selectionSort()
        {
            NodeForMyLinkedList temp = tail;

            while (true)
            {
                NodeForMyLinkedList min = temp;
                if (temp.Next == null)
                    break;
                NodeForMyLinkedList r = temp.Next;

                while (true)
                {
                    if (min.Data > r.Data)
                        min = r;
                    if (r.Next == null)
                        break;
                    r = r.Next;
                }

                int x = temp.Data;
                temp.Data = min.Data;
                min.Data = x;
                temp = temp.Next;
            }
        }
        public void InsertSorted(NodeForMyLinkedList newNode)
        {
            NodeForMyLinkedList current;
            if (tail == null)
                tail = newNode;
            else if (tail.Data >= newNode.Data)
            {
                newNode.Next = tail;
                newNode.Next.Previous = newNode;
                tail = newNode;
            }
            else
            {
                current = tail;
                while (current.Next != null && current.Next.Data < newNode.Data)
                    current = current.Next;
                newNode.Next = current.Next;
                if (current.Next != null)
                    newNode.Next.Previous = newNode;
                current.Next = newNode;
                newNode.Previous = current;
            }
        }
    }
}
