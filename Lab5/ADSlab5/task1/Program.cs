﻿using System.Diagnostics;
using task1;

Console.InputEncoding = System.Text.Encoding.UTF8;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Лабораторна робта №1. \nКупрієнко А. А. ІПЗ 22-2[1]");
MyLinkedList insertionList = new MyLinkedList();
MyLinkedList selectionList = new MyLinkedList();
Stopwatch stopwatch = new Stopwatch();
Random rand = new Random();
char gay = (char)12;
Console.WriteLine((int)gay);
for (int i = 0; i < 10000; i++)
{
    int number = rand.Next();
    if (number % 2 == 0)
    {
        selectionList.AddLast(number);
        insertionList.AddLast(number);
    }

    else
    {
        insertionList.AddFirst(number);
        selectionList.AddFirst(number);
    }
       
}
stopwatch.Start();
Sorter.InsertionSort(insertionList);
stopwatch.Stop();
Console.WriteLine($"insertion list time: {stopwatch.Elapsed.TotalMilliseconds} ms");
int[] array = new int[10000];
for (int i = 0; i < array.Length; i++)
{
    array[i] = rand.Next();
}
stopwatch.Restart();
Sorter.InsertionSort(array);
stopwatch.Stop();
Console.WriteLine($"insertion array time: {stopwatch.Elapsed.TotalMilliseconds} ms");
stopwatch.Restart();
selectionList.selectionSort();
stopwatch.Stop();
Console.WriteLine($"selection list time: {stopwatch.Elapsed.TotalMilliseconds} ms");