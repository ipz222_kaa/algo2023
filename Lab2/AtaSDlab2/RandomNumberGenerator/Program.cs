﻿using RandomNumberGenerator;
Console.InputEncoding = System.Text.Encoding.UTF8;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.WriteLine("Лабораторна робота №2\nКупрієнко А. А. ІПЗ 22-2[1] Варіант - 17\n");

const long n = 250;
const long k = 20000;

NumberGenerator generator = new NumberGenerator(DateTime.Now.Ticks % 2147483647);

long[] intervalCount = new long[n];
double[] frequencies = new double[n];
double[] intervalProbability = new double[n];
long sum = 0;
long sumSquares = 0;

for (int i = 0; i < k; i++)
{
    int rand = (int)generator.Next();
    int index = (int)Math.Abs(rand % n);
    intervalCount[index]++;
}

for (int i = 0; i < n; i++)
{
    frequencies[i] = intervalCount[i] * 100.0 / k;
    intervalProbability[i] = (double)intervalCount[i] / (k * n);
    sum += i * intervalCount[i];
    sumSquares += i * i * intervalCount[i];
}

double mean = (double)sum / k;
double variance = (double)sumSquares / k - mean * mean;
double standardDeviation = Math.Sqrt(variance);
Console.WriteLine("Кількість випадкових величин, що потрапили у кожний інтервал розміром 1:");

for (int i = 0; i < n; i++)
{
    Console.WriteLine($"{i}: {intervalCount[i]}");
}

Console.WriteLine("\nЧастота появи:");

for (int i = 0; i < n; i++)
{
    Console.WriteLine($"{i}: {frequencies[i]}");
}

Console.WriteLine("\nСтатистична імовірність появи:");

for (int i = 0; i < n; i++)
{
    Console.WriteLine($"{i}: {intervalProbability[i]:F6}");
}

Console.WriteLine($"\nМатематичне сподівання: {mean}");
Console.WriteLine($"\nДисперсія: {variance}");
Console.WriteLine($"\nСередньоквадратичне відхилення: {standardDeviation}");