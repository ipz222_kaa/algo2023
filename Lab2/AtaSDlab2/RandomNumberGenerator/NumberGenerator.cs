﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomNumberGenerator
{
    public class NumberGenerator
    {

        public long a = 2147483629;
        public long c = 2147483587;
        public long m = 2147483647;
        public long seed;

        public NumberGenerator(long seed)
        { 
            this.seed = seed;
        }

        public long Next()
        {
            this.seed = (int)(((long)this.seed * this.a + this.c) % this.m);
            return this.seed;
        }
    }
}
